<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Eloquent
{
    use Filterable, SoftDeletes, Sortable, Media;

    protected $table = 'customers';

    protected $dates = ['deleted_at'];

    protected $fillable = ['enabled'];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'phone', 'email', 'enabled', 'created_at'];
}
