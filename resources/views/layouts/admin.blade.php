<html>
    @include('partials.admin.head')

    <body class="hold-transition skin-green-light sidebar-mini fixed">
        <!-- Site wrapper -->
        <div class="wrapper">
            @include('partials.admin.header')

            @include('partials.admin.menu')

            @include('partials.admin.content')

            @include('partials.admin.footer')
        </div>
    </body>
</html>
